<?php
include 'required/header.php';
?>
    <!--        ---------------------------------- FIN MENU --------------------------------------->

    <!--        ---------------------------------- CABECERA --------------------------------------->
    <div id="conocenos-page">
        <div class="container fondo-container">
            <!--------- CABECERA Escritorio -------->
            <div id="title-black">
                <img src="img/BidoqIco.png" alt="Bidoq" />
            </div>
            <div class="row justify-content-sm-center cabecera">
                
                <h1>¿Problemas con las notificaciones electrónicas?<br>
¡EVITA SANCIONES CON E-NOTIFICA!</h1>
                <div class="col-sm-6 col-md-5 col-lg-3">
                    <span class="span1">PRUEBA 1 MES GRATIS</span>
                    <p class="span3">Simplifica la revisión y gestión de <span>las notificaciones de Hacienda, Seguridad Social y el 060.</span></p>
                    <a class="datos-tlf" href="tel:900831205">T. 900 831 205</a>
                </div>
                <div class="col-sm-2 col-lg-2 columncenter">
                        <div class="arrow"><img src="img/arrow.png" alt="flecha"></div>
                    </div>
                <div class="col-sm-5 col-md-4 col-lg-3 bloque2-section1">
                    
                        <h3>ÚSALO YA GRATIS</h3>
                        <?php 
                        include 'required/form-cabecera.php';
                        ?>
                </div>
            </div>
        </div>


        <!--        ---------------------------------- FIN CABECERA --------------------------------------->

        <!--        ---------------------------------- SLIDE --------------------------------------->
<div class="container">
        <div class="row slide-promo justify-content-sm-center">
            <p>Esta promoción es fruto de un acuerdo de colaboración entre e-notifica y bidoq. Buscamos sinergias para ofrecer los mejores servicios a tu negocio, favorecer su rendimiento y rentabilidad.<p>
        </div>


        <!--        ---------------------------------- FIN SLIDE --------------------------------------->
        <!--        ---------------------------------- ASESORIAS --------------------------------------->
            <div class="row logo-footer justify-content-sm-center">
        <img src="img/BidoqIcoFooter.png" alt="Bidoq Ico Footer"/>
    </div>
    
            <div class="row justify-content-sm-center">
                <div class="col-lg-6">
                <p class="text1">Como sabes, la administración pública envía sus avisos vía telemática. Cada profesional tiene la obligación de consultar el buzón de las distintas sedes de manera periódica y realizar los trámites que soliciten a tiempo.</p>
                    <p class="text2">Con e-notifica podrás ganar dinero ofreciendo a tus clientes la posibilidad de despreocuparse. El programa te permite:</p>
                    <p class="text4">· Gestionar desde la app las notificaciones de distintos certificados.</p>
<p class="text4">· Recibe avisos en el sistema y por e-mail cuando cualquier organismo envíe una notificación a ti o a tus clientes.</p>
<p class="text4">· Automatización de firma, descarga y avisos, para que la gestión sea rápida y tu margen de beneficio se amplíe.</p>
                    <p class="text3">E-notifica es un programa que se encarga de recibir, firmar y gestionar automáticamente las notificaciones electrónicas que reciben tus clientes de distintos organismos españoles.</p>
                    <p class="text3">Gana dinero mientras facilitas la labor a tus clientes y se aseguran de no pasar por alto ninguna notificación importante.</p>
                </div>
            </div>
        
        
        <div class="row slide-promo2 justify-content-sm-center">
            
                <div class="col-lg-9">
                    <p>Promoción válida durante el primer mes de contratación.<br>Una vez agotada la oferta, el precio del servicio es de 30€/mes.</p>
                </div>
             
        </div>


        <!--        ---------------------------------- FIN WHY --------------------------------------->
        <!--        ---------------------------------- OPINIONES --------------------------------------->


       

</div>
    <?php
include 'required/footer.php';
?>
